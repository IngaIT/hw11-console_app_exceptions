package com.family.model;

import com.family.exception.FamilyOverflowException;

import java.time.format.DateTimeFormatter;
import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size()) {
            return false;
        }
        children.get(index).setFamily(null);
        children.remove(index);
        return true;
    }

    public int countFamily() {
        return 2 + children.size();
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + (mother != null ? mother.getName() : "none") +
                ", father=" + (father != null ? father.getName() : "none") +
                ", childrenCount=" + (children != null ? children.size() : 0) +
                ", petCount=" + (pets != null ? pets.size() : 0) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }

    public String prettyFormat() {
        StringBuilder sb = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        sb.append("family:\n");
        sb.append("   mother: ").append(mother != null ? mother.toString() : "none").append("\n");
        sb.append("   father: ").append(father != null ? father.toString() : "none").append("\n");

        if (!children.isEmpty()) {
            sb.append("   children:\n");
            for (Human child : children) {
                sb.append("           ").append(child).append(": ").append(child.toString()).append("\n");
            }
        } else {
            sb.append("   children: none\n");
        }

        if (!pets.isEmpty()) {
            sb.append("   pets: ").append(pets.toString()).append("\n");
        } else {
            sb.append("   pets: none\n");
        }

        return sb.toString();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(Pet pet) {
        if (this.pets == null) {
            this.pets = new HashSet<>();
        }
        this.pets.add(pet);
    }

    public void removePets(Pet pet) {
        if (this.pets != null) {
            this.pets.remove(pet);
        }
    }

    public void bornChild(String maleName, String femaleName) {
        int maxFamilyMembers = 10;
        if (this.countFamily() >= maxFamilyMembers) {
            throw new FamilyOverflowException("Сім'я вже має максимальну кількість членів");
        }
    }

    public Family adoptChild(Human child) {
        int maxFamilyMembers = 10;
        if (this.countFamily() >= maxFamilyMembers) {
            throw new FamilyOverflowException("Сім'я вже має максимальну кількість членів");
        }

        return null;
    }
}
