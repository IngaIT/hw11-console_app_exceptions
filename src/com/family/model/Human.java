package com.family.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, Activity> schedule;
    private Family family;

    public Human() {
    }


    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<DayOfWeek, Activity> schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, long birthDate, int iq, Map<DayOfWeek, Activity> schedule, Family family) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public Human(String mother) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human human)) return false;
        return birthDate == human.birthDate && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(schedule, human.schedule) && Objects.equals(family, human.family) && Objects.equals(weekSchedule, human.weekSchedule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule, family, weekSchedule);
    }

    public enum DayOfWeek {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    public enum Activity {
        WORK, STUDY, SWIMMING, THEATRE, CINEMA, SLEEP
    }

    Map<DayOfWeek, Activity> weekSchedule = new HashMap<>();


    public void greetPet() {
        for (Pet pet : family.getPets()) {
            System.out.println("Привіт, " + pet.getNickname());
        }
    }

    public void describePet() {
        for (Pet pet : family.getPets()) {
            System.out.println("У мене є " + pet.getSpecies() + ", йому " + pet.getAge() + " років, він " + (pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий"));
        }
    }

    private long parseDate(String birthDate) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = format.parse(birthDate);
            return date.getTime();

        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid date format. Use dd/MM/yyyy.");
        }
    }

    public String describeAge() {
        long currentTimeMillis = System.currentTimeMillis();
        long diffInMillis = currentTimeMillis - birthDate;
        long years = diffInMillis / (1000L * 60 * 60 * 24 * 365);
        long months = (diffInMillis % (1000L * 60 * 60 * 24 * 365)) / (1000L * 60 * 60 * 24 * 30);
        long days = (diffInMillis % (1000L * 60 * 60 * 24 * 30)) / (1000L * 60 * 60 * 24);
        return years + " years, " + months + " months, and " + days + " days";
    }


    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String birthDateStr = format.format(new Date(birthDate));
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDateStr +
                ", iq=" + iq +
                ", schedule=" + schedule +
                ", family=" + (family != null ? "exists" : "none") +
                ", weekSchedule=" + weekSchedule +
                '}';
    }


    @Override
    protected void finalize() throws Throwable {
        System.out.println(this.toString() + " is being deleted by the Garbage Collector");
        super.finalize();
    }

    public String prettyFormat() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("name='").append(name).append("', ");
        sb.append("surname='").append(surname).append("', ");
        sb.append("birthDate='").append(birthDate).append("', ");
        sb.append("iq=").append(iq);
        if (schedule != null) {
            sb.append(", schedule={");
            for (Map.Entry<DayOfWeek, Activity> entry : schedule.entrySet()) {
                sb.append(entry.getKey()).append("=").append(entry.getValue()).append(", ");
            }
            if (!schedule.isEmpty()) {
                sb.delete(sb.length() - 2, sb.length());
            }
            sb.append("}");
        }
        sb.append("}");
        return sb.toString();
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, Activity> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, Activity> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Map<DayOfWeek, Activity> getWeekSchedule() {
        return weekSchedule;
    }

    public void setWeekSchedule(Map<DayOfWeek, Activity> weekSchedule) {
        this.weekSchedule = weekSchedule;
    }
}