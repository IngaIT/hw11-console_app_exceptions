package com.family.dao;

import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()){
            return families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index < families.size()){
            families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean saveFamily(Family family) {
        int index = families.indexOf(family);
        if (index != -1){
            families.set(index, family);
        } else {
            families.add(family);
        }
        return true;
    }

    @Override
    public List<Family> getFamiliesBiggerThan(int count) {
        return families.stream()
                .filter(family -> family.countFamily() > count)
                .collect(Collectors.toList());
    }

    @Override
    public List<Family> getFamiliesLessThan(int count) {
        return families.stream()
                .filter(family -> family.countFamily() < count)
                .collect(Collectors.toList());
    }

    @Override
    public int countFamiliesWithMemberNumber(int count) {
        return (int) families.stream()
                .filter(family -> family.countFamily() == count)
                .count();
    }

    @Override
    public void displayAllFamilies() {
        for(Family family : families) {
            System.out.println(family);
        }
    }

    @Override
    public Family createNewFamily(Human mother, Human father) {
        Family newFamily = new Family(mother, father);
        families.add(newFamily);
        return newFamily;
    }

    @Override
    public Family bornChild(int family, String manName, String womanName) {
        return null;
    }


    public Family bornChild(Family family, String maleName, String femaleName) {
        if (family == null) {
            System.out.println("Family not found!");
            return null;
        }

        Random random = new Random();
        Human child;
        if (random.nextBoolean()) {
            child = new Human(maleName, family.getFather().getSurname(), 2023);
        } else {
            child = new Human(femaleName, family.getFather().getSurname(), 2023);
        }

        family.addChild(child);
        saveFamily(family);
        return family;
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        family.getChildren().add(child);
        saveFamily(family);
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThen(int age) {
        long currentMillis = System.currentTimeMillis();
        LocalDate currentDate = LocalDate.ofEpochDay(currentMillis / (24 * 60 * 60 * 1000));
        FamilyDao familyDao = null;
        familyDao.getAllFamilies().forEach(family -> {
            List<Human> updatedChildren;
            updatedChildren = family.getChildren().stream()
                    .filter(child -> {
                        return calculateAge(child.getBirthDate(), currentDate) <= age;
                    })
                    .collect(Collectors.toList());
            family.setChildren(updatedChildren);
        });
        return true;
    }
    private int calculateAge(long birthDateMillis, LocalDate currentDate) {
        LocalDate birthDate = LocalDate.ofEpochDay(birthDateMillis / (24 * 60 * 60 * 1000));
        return Period.between(birthDate, currentDate).getYears();
    }


    @Override
    public int count() {
        return families.size();
    }

    @Override
    public Family getFamilyById(int index) {
        return getFamilyByIndex(index);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        Family family = getFamilyByIndex(familyIndex);
        return family != null ? new ArrayList<>(family.getPets()) : Collections.emptyList();
    }

    @Override
    public boolean addPets(int familyIndex, Pet pet) {
        Family family = getFamilyByIndex(familyIndex);
        if (family != null) {
            family.getPets().add(pet);
            return saveFamily(family);
        } else {
            return false;
        }
    }
}
