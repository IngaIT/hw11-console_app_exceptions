package com.family.controller;

import com.family.exception.FamilyOverflowException;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;

import java.util.List;

public class FamilyController {
    private final FamilyService familyService;
    private int index;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }


public Family getFamilyById(int index) {
    return familyService.getFamilyById(index);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public boolean saveFamily(Family family) {
        return familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int count) {
        return familyService.getFamiliesBiggerThan(count);
    }

    public List<Family> getFamiliesLessThan(int count) {
        return familyService.getFamiliesLessThan(count);
    }

    public int countFamiliesWithMemberNumber(int count) {
        return familyService.countFamiliesWithMemberNumber(count);
    }

    public Family createNewFamily(Human father, Human mother) {
        return familyService.createNewFamily(father, mother);
    }

    public Family bornChild(Family family, String manName, String womanName) {
        return familyService.bornChild(family.countFamily(), manName, womanName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public List<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public boolean addPets(int familyIndex, Pet pet) {
        return familyService.addPets(familyIndex, pet);
    }


}
