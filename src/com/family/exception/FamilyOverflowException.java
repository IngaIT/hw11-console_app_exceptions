package com.family.exception;

    public class FamilyOverflowException extends RuntimeException {
        public FamilyOverflowException(String message) {
            super(message);
        }
    }
