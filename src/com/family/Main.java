package com.family;

import com.family.controller.FamilyController;
import com.family.dao.CollectionFamilyDao;
import com.family.dao.FamilyDao;
import com.family.model.*;
import com.family.service.FamilyService;

import java.util.*;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        initializeTestData(familyController); // Заповнюємо тестовими даними

        boolean exit = false;

        while (!exit) {
            System.out.println("\nДоступні команди:");
            System.out.println("1. Заповнити тестовими даними");
            System.out.println("2. Відобразити весь список сімей");
            System.out.println("3. Відобразити список сімей, де кількість людей більша за задану");
            System.out.println("4. Відобразити список сімей, де кількість людей менша за задану");
            System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює");
            System.out.println("6. Створити нову родину");
            System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку");
            System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку");
            System.out.println("9. Видалити всіх дітей старше віку (у всіх сім'ях)");
            System.out.println("0. Вийти з програми");

            System.out.print("Введіть номер команди: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    initializeTestData(familyController);
                    break;
                case 2:
                    displayAllFamilies(familyController);
                    break;
                case 3:
                    displayFamiliesBiggerThan(familyController);
                    break;
                case 4:
                    displayFamiliesLessThan(familyController);
                    break;
                case 5:
                    countFamiliesWithMemberNumber(familyController);
                    break;
                case 6:
                    createNewFamily(familyController);
                    break;
                case 7:
                    deleteFamilyByIndex(familyController);
                    break;
                case 8:
                    editFamily(familyController);
                    break;
                case 9:
                    deleteChildrenOlderThan(familyController);
                    break;
                case 0:
                    exit = true;
                    break;
                default:
                    System.out.println("Невідома команда. Спробуйте ще раз.");
            }
        }
    }

    private static void initializeTestData(FamilyController familyController) {
        Set<String> dogHabits = new HashSet<>(Arrays.asList("їсть", "грається", "спить"));
        Dog dog = new Dog("Барбос", "Buddy", 75, dogHabits);

        Set<String> catHabits = new HashSet<>();
        DomesticCat cat = new DomesticCat(Pet.Species.CAT, "Мурзік", 2, 40, catHabits);

        Map<Human.DayOfWeek, Human.Activity> father1Schedule = new HashMap<>();
        father1Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.TUESDAY, Human.Activity.WORK);
        father1Schedule.put(Human.DayOfWeek.FRIDAY, Human.Activity.THEATRE);
        father1Schedule.put(Human.DayOfWeek.SATURDAY, Human.Activity.SWIMMING);
        Human father1 = new Human("Олег", "Іванов", 1975, 130, father1Schedule);

        Human mother1 = new Human("Олена", "Іванова", 1977);
        Human child1 = new Human("Марія", "Іванова", 2001);

        Human father2 = new Human("Андрій", "Петров", 1980);
        Human mother2 = new Human("Наталія", "Петрова", 1982);

        Map<Human.DayOfWeek, Human.Activity> child2Schedule = new HashMap<>();
        child2Schedule.put(Human.DayOfWeek.MONDAY, Human.Activity.WORK);
        child2Schedule.put(Human.DayOfWeek.WEDNESDAY, Human.Activity.STUDY);
        child2Schedule.put(Human.DayOfWeek.THURSDAY, Human.Activity.CINEMA);
        child2Schedule.put(Human.DayOfWeek.SUNDAY, Human.Activity.SLEEP);
        Human child2 = new Human("Максим", "Петров", 2005, 100, child2Schedule);

        familyController.createNewFamily(mother1, father1);
        familyController.createNewFamily(mother2, father2);
        familyController.adoptChild(familyController.getFamilyById(0), child1);
        familyController.adoptChild(familyController.getFamilyById(1), child2);
        familyController.addPets(0, dog);
        familyController.addPets(1, cat);
    }

    private static void displayAllFamilies(FamilyController familyController) {
        System.out.println("\nСписок усіх сімей:");
        familyController.displayAllFamilies();
    }

    private static void displayFamiliesBiggerThan(FamilyController familyController) {
        System.out.print("\nВведіть цікаву кількість людей: ");
        int count = scanner.nextInt();
        List<Family> families = familyController.getFamiliesBiggerThan(count);
        System.out.println("\nСписок сімей, де кількість людей більша за " + count + ":");
        families.forEach(family -> System.out.println(family.prettyFormat()));
    }

    private static void displayFamiliesLessThan(FamilyController familyController) {
        System.out.print("\nВведіть цікаву кількість людей: ");
        int count = scanner.nextInt();
        List<Family> families = familyController.getFamiliesLessThan(count);
        System.out.println("\nСписок сімей, де кількість людей менша за " + count + ":");
        families.forEach(System.out::println);
    }

    private static void countFamiliesWithMemberNumber(FamilyController familyController) {
        System.out.print("\nВведіть цікаву кількість членів сім'ї: ");
        int count = scanner.nextInt();
        int numFamilies = familyController.countFamiliesWithMemberNumber(count);
        System.out.println("Кількість сімей, де кількість членів дорівнює " + count + ": " + numFamilies);
    }

    private static void createNewFamily(FamilyController familyController) {
        System.out.println("\nСтворення нової родини:");
        scanner.nextLine(); // Чистимо буфер
        System.out.print("Введіть ім'я матері: ");
        String motherName = scanner.nextLine();
        System.out.print("Введіть прізвище матері: ");
        String motherSurname = scanner.nextLine();
        System.out.print("Введіть рік народження матері: ");
        int motherBirthYear = scanner.nextInt();
        System.out.print("Введіть місяць народження матері: ");
        int motherBirthMonth = scanner.nextInt();
        System.out.print("Введіть день народження матері: ");
        int motherBirthDay = scanner.nextInt();
        System.out.print("Введіть iq матері: ");
        int motherIq = scanner.nextInt();

        scanner.nextLine(); // Чистимо буфер
        System.out.print("Введіть ім'я батька: ");
        String fatherName = scanner.nextLine();
        System.out.print("Введіть прізвище батька: ");
        String fatherSurname = scanner.nextLine();
        System.out.print("Введіть рік народження батька: ");
        int fatherBirthYear = scanner.nextInt();
        System.out.print("Введіть місяць народження батька: ");
        int fatherBirthMonth = scanner.nextInt();
        System.out.print("Введіть день народження батька: ");
        int fatherBirthDay = scanner.nextInt();
        System.out.print("Введіть iq батька: ");
        int fatherIq = scanner.nextInt();

        Human mother = new Human(motherName, motherSurname, motherBirthYear, motherIq, null);
        Human father = new Human(fatherName, fatherSurname, fatherBirthYear, fatherIq, null);

        Family newFamily = familyController.createNewFamily(mother, father);
        System.out.println("\nНова родина створена:");
        System.out.println(newFamily.prettyFormat());
    }

    private static void deleteFamilyByIndex(FamilyController familyController) {
        System.out.print("\nВведіть індекс сім'ї, яку бажаєте видалити: ");
        int index = scanner.nextInt();
        if (familyController.deleteFamilyByIndex(index - 1)) {
            System.out.println("Сім'ю за індексом " + index + " видалено успішно.");
        } else {
            System.out.println("Сім'ю за індексом " + index + " не знайдено.");
        }
    }

    private static void editFamily(FamilyController familyController) {
        System.out.print("\nВведіть індекс сім'ї, яку бажаєте редагувати: ");
        int index = scanner.nextInt();

        boolean backToMainMenu = false;
        while (!backToMainMenu) {
            System.out.println("\nРедагування сім'ї:");
            System.out.println("1. Народити дитину");
            System.out.println("2. Усиновити дитину");
            System.out.println("3. Повернутися до головного меню");

            System.out.print("Введіть номер команди: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    bornChild(familyController, index);
                    break;
                case 2:
                    adoptChild(familyController, index);
                    break;
                case 3:
                    backToMainMenu = true;
                    break;
                default:
                    System.out.println("Невідома команда. Спробуйте ще раз.");
            }
        }
    }

    private static void bornChild(FamilyController familyController, int familyIndex) {
        System.out.print("Введіть ім'я дитини (хлопчика): ");
        String boyName = scanner.next();
        System.out.print("Введіть ім'я дитини (дівчинки): ");
        String girlName = scanner.next();
        Family family = familyController.getFamilyById(familyIndex - 1);
        familyController.bornChild(family, boyName, girlName);
        System.out.println("\nДитину успішно народжено:");
        System.out.println(family.prettyFormat());
    }

    private static void adoptChild(FamilyController familyController, int familyIndex) {
        scanner.nextLine();
        System.out.print("Введіть ПІБ дитини: ");
        String childName = scanner.nextLine();
        System.out.print("Введіть рік народження дитини: ");
        int childBirthYear = scanner.nextInt();
        System.out.print("Введіть інтелект дитини: ");
        int childIq = scanner.nextInt();

        Family family = familyController.getFamilyById(familyIndex - 1);
        Human child = new Human(childName, family.getFather().getSurname(), childBirthYear, childIq, null);
        familyController.adoptChild(family, child);

        System.out.println("\nДитину успішно усиновлено:");
        System.out.println(family.prettyFormat());
    }

    private static void deleteChildrenOlderThan(FamilyController familyController) {
        System.out.print("Введіть вік, старший за який бажаєте видалити дітей: ");
        int age = scanner.nextInt();
        familyController.deleteAllChildrenOlderThan(age);
        System.out.println("\nВсі діти старше " + age + " років видалені.");
    }
}

