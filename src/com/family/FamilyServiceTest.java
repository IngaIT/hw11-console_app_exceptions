package com.family;

import com.family.dao.FamilyDao;
import com.family.model.Family;
import com.family.model.Human;
import com.family.model.Pet;
import com.family.service.FamilyService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FamilyServiceTest {
    @Mock
    private FamilyDao familyDao;

    private FamilyService familyService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        familyService = new FamilyService(familyDao);
    }


    @Test
    public void testGetAllFamilies() {
        List<Family> families = Arrays.asList(new Family(new Human("Mother1"), new Human("Father1")),
                new Family(new Human("Mother2"), new Human("Father2")));
        when(familyDao.getAllFamilies()).thenReturn(families);
        List<Family> result = familyService.getAllFamilies();
        assertEquals(families, result);
    }



    @Test
    public void testAdoptChild() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        Human child = new Human("Child");
        when(familyDao.getFamilyById(anyInt())).thenReturn(family);
        when(familyDao.saveFamily(family)).thenReturn(true);

        Family result = familyService.adoptChild(family, child);
        assertEquals(family, result);
        assertTrue(family.getChildren().contains(child));
    }

    @Test
    public void testGetFamilyById() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        when(familyDao.getFamilyById(0)).thenReturn(family);

        Family result = familyService.getFamilyById(0);
        assertEquals(family, result);
    }

    @Test
    public void testDeleteFamilyByIndex() {
        when(familyDao.deleteFamily(0)).thenReturn(true);
        boolean result = familyService.deleteFamilyByIndex(0);
        assertTrue(result);
    }

    @Test
    public void testCount() {
        when(familyDao.count()).thenReturn(2);
        int result = familyService.count();
        assertEquals(2, result);
    }

    @Test
    public void testGetPets() {
        List<Pet> pets = Arrays.asList(new Pet("Dog"), new Pet("Cat"));
        when(familyDao.getPets(0)).thenReturn(pets);
        List<Pet> result = familyService.getPets(0);
        assertEquals(pets, result);
    }

    @Test
    public void testAddPets() {
        Pet pet = new Pet("Dog");
        when(familyDao.addPets(0, pet)).thenReturn(true);
        boolean result = familyService.addPets(0, pet);
        assertTrue(result);
    }

    /*@Test
    public void testBornChild() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        when(familyDao.bornChild(family, "maleName", "femaleName")).thenReturn(family);
        Family result = familyService.bornChild(family, "maleName", "femaleName");
        assertEquals(family, result);
    }*/

    @Test
    public void testDeleteAllChildrenOlderThan() {
        doAnswer(invocation -> {
            int age = invocation.getArgument(0);
            if (age == 18) {
                return null; // Do nothing for age 18
            } else {
                throw new RuntimeException("Unexpected call to deleteAllChildrenOlderThen");
            }
        }).when(familyDao).deleteAllChildrenOlderThen(ArgumentMatchers.anyInt());

        familyService.deleteAllChildrenOlderThan(18);
        verify(familyDao, times(1)).deleteAllChildrenOlderThen(18);
    }

    @Test
    public void testSaveFamily() {
        Family family = new Family(new Human("Mother"), new Human("Father"));
        when(familyDao.saveFamily(family)).thenReturn(true);
        boolean result = familyService.saveFamily(family);
        assertTrue(result);
    }

}

